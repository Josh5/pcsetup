#!/bin/bash
#
# @Author: Josh Sunnex
# @Date:   2018-08-13 12:54:13
# @Last Modified by:   Josh S
# @Last Modified time: 2018-09-03 09:41:22
# 

OS_ID=$(lsb_release -si);
RELEASE=$(lsb_release -sr);
RELEASE_MAJOR=$(lsb_release -sr | awk -F '.' '{print $1}');
CODENAME=$(lsb_release -sc);
DATE_STAMP=$(date +"%Y-%m-%d_%H-%M-%S");
MAIN_UBUNTU_SRC="http://nz.archive.ubuntu.com/ubuntu/";


while [[ true ]]; do
	echo 
	echo "Detected The following OS:"
	echo "  ${OS_ID} ${RELEASE} (${CODENAME})"
	echo 
    echo -e "\e[93m";
	echo "You are about to completely reset your apt sources. This will store a backup in /etc/apt.backup.${DATE_STAMP}.tar.gz";
	echo "Default ubuntu sources will be set to ${MAIN_UBUNTU_SRC}  ${CODENAME}"
	echo 
    read -n1 -p "Are you sure you wish to continue? (y|n)  " an 
    case ${an} in  
        y|Y)
            echo -e "\e[0m";
            break
            ;; 
        n|N) 
            exit 0;
            ;; 
        *) 
            echo
            echo 'Please select either "y" or "n"'
            ;; 
    esac
    echo -e "\e[0m";
    echo 
done
echo -e "\e[0m";

# Manipulate OS values to allow for distros like LinuxMint
OS_ID=$(echo "${OS_ID}" | awk '{print tolower($0)}');
if [[ ${OS_ID} =~ "mint" ]]; then
    OS_ID="linuxmint";
    # Get Ubuntu codename for this release
    CODENAME=$(cat /etc/os-release | grep UBUNTU_CODENAME | awk -F'=' '{print $2}');
fi
APT_ARGS="-y";
if [[ ${OS_ID} != "linuxmint" && "${RELEASE_MAJOR}" -ge "18" ]]; then
    APT_ARGS+=" -n";
fi

stage_header() {
    echo -e "\e[1;34m"
    echo -e "####################################################################"
    echo -e "##                "
    echo -e "##       ${1}";
    echo -e "##                "
    echo -e "####################################################################"
    echo -e "\e[0m"
}

#------------------------------------------------------------------------------#
#                         BACKUP CURRENT SOURCES LIST                          #
#------------------------------------------------------------------------------#
stage_header "Backing up previous apt sources...";

echo "Backup current apt sources...";
if[[ ${OS_ID} == "linuxmint" ]]; then
    cp -fv /etc/apt/sources.list.d/official-package-repositories.list /tmp/official-package-repositories.list;
    cp -rfv /etc/apt/preferences.d /tmp/preferences.d;
fi
sudo tar czf /etc/apt.backup.${DATE_STAMP}.tar.gz -C /etc/apt .

echo "Removing current apt sources...";
sudo rm -rf /etc/apt/sources.list.d
sudo rm -rf /etc/apt/preferences.d
sudo rm -rf /etc/apt/trusted.gpg.d
sudo rm -rf /etc/apt/sources*
sudo rm -rf /etc/apt/trusted*

sudo mkdir -p /etc/apt/sources.list.d
sudo mkdir -p /etc/apt/preferences.d
sudo mkdir -p /etc/apt/trusted.gpg.d
sudo touch /etc/apt/sources.list




#------------------------------------------------------------------------------#
#                            OFFICIAL UBUNTU REPOS                             #
#------------------------------------------------------------------------------#
stage_header "Adding offical ubuntu repos...";

if [[ ${OS_ID} == "linuxmint" ]]; then
    sudo cp -fv /tmp/official-package-repositories.list  /etc/apt/sources.list.d/official-package-repositories.list
    sudo cp -rfv /tmp/preferences.d/*  /etc/apt/preferences.d/
else
    set -x

    ###### Ubuntu Main Repos
    sudo add-apt-repository ${APT_ARGS} "deb ${MAIN_UBUNTU_SRC} ${CODENAME} main restricted universe multiverse"

    ###### Ubuntu Update Repos
    sudo add-apt-repository ${APT_ARGS} "deb ${MAIN_UBUNTU_SRC} ${CODENAME}-security main restricted universe multiverse"
    sudo add-apt-repository ${APT_ARGS} "deb ${MAIN_UBUNTU_SRC} ${CODENAME}-updates main restricted universe multiverse"
    sudo add-apt-repository ${APT_ARGS} "deb ${MAIN_UBUNTU_SRC} ${CODENAME}-proposed main restricted universe multiverse"
    sudo add-apt-repository ${APT_ARGS} "deb ${MAIN_UBUNTU_SRC} ${CODENAME}-backports main restricted universe multiverse"

    set +x
fi


#------------------------------------------------------------------------------#
#                           UNOFFICIAL UBUNTU REPOS                            #
#------------------------------------------------------------------------------#
stage_header "Adding unoffical repos...";
set -x

#### NVIDIA Proprietary Drivers - 
sudo add-apt-repository ${APT_ARGS} ppa:graphics-drivers/ppa

#### UKUU - https://github.com/teejee2008/ukuu
sudo apt-add-repository ${APT_ARGS} ppa:teejee2008/ppa

#### Gimp PPA - https://launchpad.net/~otto-kesselgulasch/+archive/gimp
sudo add-apt-repository ${APT_ARGS} ppa:otto-kesselgulasch/gimp

#### Java - https://launchpad.net/~webupd8team/+archive/ubuntu/java
sudo add-apt-repository ${APT_ARGS} ppa:webupd8team/java

#### Sublime Text - https://launchpad.net/~webupd8team/+archive/ubuntu/sublime-text-3
#echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo add-apt-repository ${APT_ARGS} "deb [arch=amd64] https://download.sublimetext.com/ apt/stable/"

#### OpenRazer - https://openrazer.github.io/#download
sudo add-apt-repository ${APT_ARGS} ppa:openrazer/stable

#### razergenie - https://github.com/z3ntu/RazerGenie
if [[ ${OS_ID} != "linuxmint" ]]; then
    wget -q -O - https://download.opensuse.org/repositories/hardware:razer/xUbuntu_${RELEASE}/Release.key | sudo apt-key add -
    sudo add-apt-repository ${APT_ARGS} "deb http://download.opensuse.org/repositories/hardware:/razer/xUbuntu_${RELEASE}/ /"
fi

#### Google Drive - https://github.com/astrada/google-drive-ocamlfuse/wiki/Installation
sudo add-apt-repository ${APT_ARGS} ppa:alessandro-strada/ppa

#### Ubuntu Make - https://wiki.ubuntu.com/ubuntu-make
sudo add-apt-repository ${APT_ARGS} ppa:lyzardking/ubuntu-make

#### PHP - https://launchpad.net/~ondrej/+archive/ubuntu/php
sudo add-apt-repository ${APT_ARGS} ppa:ondrej/php

#### DBeaver - https://dbeaver.jkiss.org/download/
sudo add-apt-repository ${APT_ARGS} ppa:serge-rider/dbeaver-ce

### Docker - https://docs.docker.com/install/linux/docker-ce/ubuntu/
sudo add-apt-repository ${APT_ARGS} "deb [arch=amd64] https://download.docker.com/linux/ubuntu ${CODENAME} stable"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#### Wine - https://wiki.winehq.org/Ubuntu
sudo add-apt-repository ${APT_ARGS} ppa:ubuntu-wine/ppa

#### Vineyard - http://vineyardproject.org/download/
sudo add-apt-repository ${APT_ARGS} ppa:cybolic/ppa

set +x


#------------------------------------------------------------------------------#
#                              ADD MISSING KEYS                                #
#------------------------------------------------------------------------------#
stage_header "Add any missing keys...";

ADDED_KEYS="";
for MISSING_KEY in $(sudo apt-get update | grep "NO_PUBKEY" | sed "s/.*NO_PUBKEY //"); do 
    if [[ "${ADDED_KEYS}" =~ "${MISSING_KEY}" ]]; then
        continue;
    fi
	echo -e "\nProcessing key: ${MISSING_KEY}"; 
	sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ${MISSING_KEY};
    ADDED_KEYS="${ADDED_KEYS} ${MISSING_KEY}";
done

