#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2020-03-28 14:07:18
# 


# Read the src package data
read_src_package_data() {
    PACKAGE_INFO=${@};
    VERSION='null';
    DOWNLOAD_LINK='null';
    DEST_FILE='null';

    SRC_SRC=$(read_json_data "${PACKAGE_INFO}" ".source");
    if [[ "${SRC_SRC}" == "git" ]]; then
        # Return url and branch name
        DOWNLOAD_LINK=$(read_json_data "${PACKAGE_INFO}" ".git_clone_url");
        VERSION=$(read_json_data "${PACKAGE_INFO}" ".git_clone_branch");

        # Overwrite destination file name
        DEST_FILE="";
    fi
    if [[ "${SRC_SRC}" == "github" ]]; then
        # Get download form github
        GITHUB_PROJECT=$(read_json_data "${PACKAGE_INFO}" ".github_project_url");
        GITHUB_RELEASE_EXT=$(read_json_data "${PACKAGE_INFO}" ".github_release_extension");

        # Get latest version information from github
        VERSION=$(github_get_latest_release "${GITHUB_PROJECT}");
        if [[ -z ${VERSION} ]]; then
            throw_exception "Unable to find latest src version on git for - ${PACKAGE_NAME}";
        fi

        # Get matching download link from github
        DOWNLOAD_LINK=$(github_get_release_download_url "${GITHUB_PROJECT}" "${VERSION}" "${GITHUB_RELEASE_EXT}");

        # Overwrite destination file name
        DEST_FILE="${DOWNLOAD_LINK##*/}";
    fi
    if [[ "${SRC_SRC}" == "tar" ]]; then
        TAR_URL=$(read_json_data "${PACKAGE_INFO}" ".tar_url");
        TAR_EXT=$(read_json_data "${PACKAGE_INFO}" ".tar_extension");
        TAR_VERSION_CMD=$(read_json_data "${PACKAGE_INFO}" ".tar_version_cmd");

        # Get latest version information from github
        VERSION=$(bash -c "${TAR_VERSION_CMD}");
        if [[ -z ${VERSION} ]]; then
            throw_exception "Unable to find latest src version for - ${PACKAGE_NAME}";
        fi

        # Replace version information in download URL
        DOWNLOAD_LINK=$(echo "${TAR_URL}" | sed 's|${VERSION}|'${VERSION}'|g');

        # Overwrite destination file name
        DEST_FILE=${PACKAGE_BIN}-${VERSION}.${TAR_EXT};
    fi
    if [[ "${SRC_SRC}" == "zip" ]]; then
        ZIP_URL=$(read_json_data "${PACKAGE_INFO}" ".zip_url");
        ZIP_VERSION_CMD=$(read_json_data "${PACKAGE_INFO}" ".zip_version_cmd");

        # Get latest version information from github
        VERSION=$(bash -c "${ZIP_VERSION_CMD}");
        if [[ -z ${VERSION} ]]; then
            throw_exception "Unable to find latest src version for - ${PACKAGE_NAME}";
        fi

        # Replace version information in download URL
        DOWNLOAD_LINK=$(echo "${ZIP_URL}" | sed 's|${VERSION}|'${VERSION}'|g');

        # Overwrite destination file name
        DEST_FILE=${PACKAGE_BIN}-${VERSION}.zip;
    fi
    echo "$(echo ${VERSION} | base64 -w0) $(echo ${DOWNLOAD_LINK} | base64 -w0) $(echo ${DEST_FILE} | base64 -w0)";
}
