#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2020-03-28 14:28:39
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


# Source the utils.sh file for required helper functions
source ${SCRIPT_DIR}/utils.sh


# Install an src package
install_src_package() {
    FILE_PATH="";
    FORCE_REINSTALL='false';
    for ARG in ${@}; do
        if [[ "${ARG}" == "--reinstall" || "${ARG}" == "-r" ]]; then
            FORCE_REINSTALL='true';
            continue;
        else
            FILE_PATH="${ARG}";
            continue;
        fi
    done

    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    PACKAGE_BIN=$(read_package_binary "${FILE_PATH}");
    PACKAGE_DESCRIPTION=$(read_package_description "${FILE_PATH}");
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.src_package_info");
    SRC_SRC=$(read_json_data "${PACKAGE_INFO}" ".source");
    SRC_DESKTOP_SHORTCUT=$(read_json_data "${PACKAGE_INFO}" ".desktop_shortcut");
    SRC_ICON_URL=$(read_json_data "${PACKAGE_INFO}" ".icon_url");
    SRC_ICON_DEST=$(read_json_data "${PACKAGE_INFO}" ".icon_dest");

    print_stage_header "Installing src package - '${PACKAGE_NAME}'"
    echo

    # Ensure we have all required config information
    if [[ "${PACKAGE_BIN}" == 'null' ]]; then
        throw_exception "Package configuration incomplete. Missing key: '.bin'";
    fi
    if [[ "${SRC_SRC}" == 'null' ]]; then
        throw_exception "Package configuration incomplete. Missing key: '.installation.src_package_info.source'";
    fi

    # Make destination
    DEST=${HOME}/.local/share/${PACKAGE_BIN};
    bash -c "mkdir -p ${DEST}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to make destination folder - ${DEST}";
    fi

    # Set defaults for destinations of src, icon and version number
    VERSION="unknown"
    DEST_FILE=${DEST}/${PACKAGE_BIN}-${VERSION}.src;
    DEST_ICON=${DEST}/${SRC_ICON_DEST};

    # Get source information
    i=1;
    for item in $(read_src_package_data "${PACKAGE_INFO}"); do
        item="$(echo ${item} | base64 --decode)";
        if [[ "${i}" == "1" ]]; then
            if [[ "${item}" != 'null' ]]; then
                VERSION="${item}";
            fi
        fi
        if [[ "${i}" == "2" ]]; then
            if [[ "${item}" != 'null' ]]; then
                DOWNLOAD_LINK="${item}";
            fi
        fi
        if [[ "${i}" == "3" ]]; then
            if [[ "${item}" != 'null' ]]; then
                DEST_FILE="${DEST}/${item}";
            fi
        fi
        ((i++))
    done

    # Ensure we have a link
    if [[ -z ${DOWNLOAD_LINK} ]]; then
        throw_exception "Failed to find src download link for - ${PACKAGE_NAME}";
    fi

    # Check if we already have the latest version
    if [[ -e ${DEST_FILE} && -z ${FORCE_REINSTALL} ]]; then
        echo "Application source ${PACKAGE_NAME} is already the newest version (${VERSION})";
        return;
    fi

    # Download package src
    if [[ "${SRC_SRC}" == 'git' ]]; then
        echo "Cloning ${PACKAGE_NAME} src from ${DOWNLOAD_LINK}";
        if [[ -e ${DEST} ]]; then
            # Clean old files branch
            rm -rf ${DEST_FILE}
        fi
        CMD="git clone --depth=1 ${DOWNLOAD_LINK} ${DEST}";
        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to clone src for - ${PACKAGE_NAME}";
        fi
        echo
    else
        echo "Downloading ${PACKAGE_NAME} src from ${DOWNLOAD_LINK}";
        CMD="curl -L '${DOWNLOAD_LINK}' --output '${DEST_FILE}'";
        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to download src for - ${PACKAGE_NAME}";
        fi
        echo
    fi

    # Extract tar
    if [[ "${DEST_FILE}" == *".tar."* ]]; then
        pushd ${DEST};

        if [[ "${DEST_FILE}" == *".tar.gz" ]]; then
            CMD="tar xfz ${DEST_FILE} --strip 1 -C ./";
        fi
        if [[ "${DEST_FILE}" == *".tar.bz2" ]]; then
            CMD="tar xjf ${DEST_FILE} --strip 1 -C ./";
        fi
        if [[ "${DEST_FILE}" == *".tar.xz" ]]; then
            CMD="tar xaf ${DEST_FILE} --strip 1 -C ./";
        fi

        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to extract tar file - ${DEST_FILE}";
        fi
        echo

        popd;
        echo
    fi

    # Extract zip
    if [[ "${DEST_FILE}" == *".zip" ]]; then
        pushd ${DEST};

        CMD="unzip -o ${DEST_FILE} -d ./";

        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to extract zip file - ${DEST_FILE}";
        fi
        echo

        popd;
        echo
    fi

    # Download package icon (if one is configured)
    if [[ "${SRC_ICON_URL}" != 'null' && "${SRC_ICON_DEST}" != 'null' ]]; then
        echo "Downloading ${PACKAGE_NAME} icon from ${SRC_ICON_URL}";
        CMD="curl -L '${SRC_ICON_URL}' --output '${DEST_ICON}'";
        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to download icon for - ${PACKAGE_NAME}";
        fi
        echo
    fi

    # Build source package
    if [[ -e "${DEST_FILE}" ]]; then
        pushd ${DEST};

        BUILD_COMMANDS=$(read_json_data "${PACKAGE_INFO}" ".build_commands");
        for row in $(echo "${BUILD_COMMANDS}" | jq -r '.[] | @base64'); do
            CMD="$(echo ${row} | base64 --decode)";
            echo ${CMD};
            echo
            bash -c "${CMD}";
            if [[ $? -ne 0 ]]; then
                throw_exception "Failed to execute command - ${CMD}";
            fi
        done

        popd;
        echo
    fi

    # Write a start menu link for gnome
    if [[ "${SRC_DESKTOP_SHORTCUT:-null}" != 'null' && ! $(grep -ri "${DEST}" ${HOME}/.local/share/applications/) ]]; then
        # Set the exec command
        if [[ -e "${HOME}/.local/bin/${PACKAGE_BIN}" ]]; then
            LAUNCHER=${HOME}/.local/bin/${PACKAGE_BIN};
        fi
        mkdir -p ${HOME}/.local/share/applications
        menu_shortcut=${HOME}/.local/share/applications/${SRC_DESKTOP_SHORTCUT};
        touch ${menu_shortcut};
        echo '[Desktop Entry]' > ${menu_shortcut};
        echo 'Name='${PACKAGE_NAME} >> ${menu_shortcut};
        echo 'Exec="'${LAUNCHER}'" %U' >> ${menu_shortcut};
        echo 'Comment="'${PACKAGE_DESCRIPTION}'"' >> ${menu_shortcut};
        echo 'Icon='${DEST_ICON} >> ${menu_shortcut};
        echo 'Type=Application' >> ${menu_shortcut};
        echo 'TryExec='${LAUNCHER} >> ${menu_shortcut};
        echo 'Terminal=false' >> ${menu_shortcut};
        chmod 755 ${menu_shortcut};
        echo
    fi

    print_stage_success_done;
}


install_src_package ${@};
