#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 09:47:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


# Check the status of the package
check_installed_rpm_package() {
    FILE_PATH=${@};
    if [[ -z ${YUM_INSTALL_CMD} ]]; then
        echo "false";
        return;
    fi

    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.rpm_package_info");
    RPM_PACKAGE_NAME=$(read_json_data "${PACKAGE_INFO}" ".rpm_installed_package_name" | tr -d '"');
    if [[ "${RPM_PACKAGE_NAME}" != 'null' && $(rpm -qa "${RPM_PACKAGE_NAME}" 2> /dev/null) ]]; then
        echo "true";
        return;
    fi
    echo "false";
}


check_installed_rpm_package "${@}";

