#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done



# Install pacman repo and GPG key
install_pacman_package_repo() {
    FILE_PATH=${@};
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.pacman_package_info" );
    PACKAGE_REPO_NAME=$(read_json_data "${PACKAGE_INFO}" ".package_repo_name" | tr -d '"');
    PACKAGE_REPO_SERVER=$(read_json_data "${PACKAGE_INFO}" ".package_repo_server" | tr -d '"');
    PACKAGE_REPO_KEY=$(read_json_data "${PACKAGE_INFO}" ".package_repo_key" | tr -d '"');
    PACKAGE_REPO_KEY_ID=$(read_json_data "${PACKAGE_INFO}" ".package_repo_key_id" | tr -d '"');
    if [[ "${PACKAGE_REPO_NAME}" != 'null' && "${PACKAGE_REPO_SERVER}" != 'null' ]]; then
        # Check if it is already installed
        grep -Fhri "${PACKAGE_REPO_NAME}" /etc/pacman.conf > /dev/null 2>&1
        if [[ $? -ne 0 ]]; then
            if [[ "${PACKAGE_REPO_KEY}" != 'null' ]]; then
                print_stage_header "Installing pacman gpg key - '${PACKAGE_REPO_KEY}'"
                echo
                CMD=$(echo "${PACMAN_INSTALL_REPO_KEY_CMD}" | sed "s|{@}|${PACKAGE_REPO_KEY}|");
                echo "${CMD}";
                echo
                bash -c "${CMD}";
                if [[ $? -ne 0 ]]; then
                    throw_exception "Failed to install pacman gpg key - ${PACKAGE_REPO_KEY}";
                fi
                print_stage_success_done;
            fi
            if [[ "${PACKAGE_REPO_KEY_ID}" != 'null' ]]; then
                print_stage_header "Installing pacman gpg key id - '${PACKAGE_REPO_KEY}'"
                echo
                CMD=$(echo "${PACMAN_INSTALL_REPO_KEY_ID_CMD}" | sed "s|{@}|${PACKAGE_REPO_KEY_ID}|");
                echo "${CMD}";
                echo
                bash -c "${CMD}";
                if [[ $? -ne 0 ]]; then
                    throw_exception "Failed to install pacman gpg key id - ${PACKAGE_REPO_KEY}";
                fi
                print_stage_success_done;
            fi
            print_stage_header "Installing pacman repo - '${PACKAGE_REPO_NAME}'"
            echo
            CMD="echo [${PACKAGE_REPO_NAME}] | ${SUDO} tee -a /etc/pacman.conf";
            bash -c "${CMD}";
            if [[ $? -ne 0 ]]; then
                throw_exception "Failed to install pacman repo - ${PACKAGE_REPO_SERVER}";
            fi
            CMD="echo Server = ${PACKAGE_REPO_SERVER} | ${SUDO} tee -a /etc/pacman.conf";
            bash -c "${CMD}";
            if [[ $? -ne 0 ]]; then
                throw_exception "Failed to install pacman repo - ${PACKAGE_REPO_SERVER}";
            fi
            echo
            echo
            echo ${PACMAN_REFRESH_REPOS_CMD};
            echo
            bash -c "${PACMAN_REFRESH_REPOS_CMD}";
            if [[ $? -ne 0 ]]; then
                throw_exception "Failed to install pacman repo - ${PACKAGE_REPO_SERVER}";
            fi
            print_stage_success_done;
        fi
    fi
}

# Install a package via pacman
install_pacman_package() {
    FILE_PATH=${@};
    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    PACKAGE_BIN=$(read_package_binary "${FILE_PATH}");
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.pacman_package_info");
    PACMAN_PACKAGES=$(read_json_data "${PACKAGE_INFO}" ".pacman_packages" | tr -d '"');

    # Check if we need to add a repo first
    install_pacman_package_repo "${FILE_PATH}";

    print_stage_header "Installing package via pacman - '${PACKAGE_NAME}'";
    echo

    # Check if we are forcing a reinstall of the package
    if [[ ! -z ${FORCE_REINSTALL} ]]; then
        PACMAN_INSTALL_CMD="${PACMAN_INSTALL_CMD} -c "
    fi

    CMD="${PACMAN_INSTALL_CMD} ${PACMAN_PACKAGES}";
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to install packages via pacman - ${APT_PACKAGES}";
    fi
    print_stage_success_done;
}


install_pacman_package ${@};
