#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


# Install apt repo and key
install_apt_package_repo() {
    FILE_PATH=${@};
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.apt_package_info" );
    PACKAGE_REPO_SERVER=$(read_json_data "${PACKAGE_INFO}" ".package_repo_server" | tr -d '"');
    PACKAGE_REPO_KEY=$(read_json_data "${PACKAGE_INFO}" ".package_repo_key" | tr -d '"');
    if [[ "${PACKAGE_REPO_SERVER}" != 'null' ]]; then
        PACKAGE_REPO_SERVER=$(echo "${PACKAGE_REPO_SERVER}" \
                | sed 's|${OS_ID}|'${OS_ID}'|' \
                | sed 's|${RELEASE}|'${RELEASE}'|' \
                | sed 's|${CODENAME}|'${CODENAME}'|'
            );
        echo ${PACKAGE_REPO_SERVER}
        grep -Fhri "${PACKAGE_REPO_SERVER}" /etc/apt/* > /dev/null 2>&1
        if [[ $? -ne 0 ]]; then
            if [[ "${PACKAGE_REPO_KEY}" != 'null' ]]; then
                PACKAGE_REPO_KEY=$(echo "${PACKAGE_REPO_KEY}" \
                        | sed 's|${OS_ID}|'${OS_ID}'|' \
                        | sed 's|${RELEASE}|'${RELEASE}'|' \
                        | sed 's|${CODENAME}|'${CODENAME}'|'
                    );
                print_stage_header "Installing apt repo key - '${PACKAGE_REPO_KEY}'"
                echo
                CMD=$(echo "${APT_INSTALL_REPO_KEY_CMD}" | sed "s|{@}|${PACKAGE_REPO_KEY}|");
                echo "${CMD}";
                echo
                bash -c "${CMD}";
                if [[ $? -ne 0 ]]; then
                    throw_exception "Failed to install apt repo key - ${PACKAGE_REPO_KEY}";
                fi
                print_stage_success_done;
            fi
            print_stage_header "Installing apt repo - '${PACKAGE_REPO_SERVER}'"
            echo
            CMD="${APT_INSTALL_REPO_CMD} '${PACKAGE_REPO_SERVER}'";
            echo ${CMD};
            echo
            bash -c "${CMD}";
            if [[ $? -ne 0 ]]; then
                throw_exception "Failed to install apt repo - ${PACKAGE_REPO_SERVER}";
            fi
            echo
            CMD="${SUDO} apt-get update";
            echo ${CMD};
            echo
            bash -c "${CMD}";
            if [[ $? -ne 0 ]]; then
                throw_exception "Failed to update apt repo post install - ${PACKAGE_REPO_SERVER}";
            fi
            print_stage_success_done;
        fi
    fi
}

# Write any package specific configuration
write_apt_package_configs() {
    FILE_PATH=${@};
    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    PACKAGE_BIN=$(read_package_binary "${FILE_PATH}");
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.apt_package_info" );
    PACKAGE_CONFIG_CMDS=$(read_json_data "${PACKAGE_INFO}" ".apt_config_commands");
    if [[ "${PACKAGE_CONFIG_CMDS}" != 'null' ]]; then
        print_stage_header "Running post install configurations"
        for row in $(echo "${PACKAGE_CONFIG_CMDS}" | jq -r '.[] | @base64'); do
            CMD="$(echo ${row} | base64 --decode)";
            CMD=$(echo "${CMD}" | sed 's|${SUDO}|'${SUDO}'|');
            echo ${CMD};
            echo
            bash -c "${CMD}";
            if [[ $? -ne 0 ]]; then
                throw_exception "Failed to run post-install config command for package - ${PACKAGE_NAME}";
            fi
            print_stage_success_done;
        done
    fi
}

# Install a package via apt
install_apt_package() {
    FILE_PATH="";
    FORCE_REINSTALL='false';
    for ARG in ${@}; do
        if [[ "${ARG}" == "--reinstall" || "${ARG}" == "-r" ]]; then
            FORCE_REINSTALL='true';
            continue;
        else
            FILE_PATH="${ARG}";
            continue;
        fi
    done

    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.apt_package_info" );
    APT_PACKAGES=$(read_json_data "${PACKAGE_INFO}" ".apt_packages" | tr -d '"');

    # Check if we need to add a repo first
    install_apt_package_repo "${FILE_PATH}";

    print_stage_header "Installing package(s) via apt-get - '${APT_PACKAGES}'";
    echo

    # Check if we are forcing a reinstall of the package
    if [[ ! -z ${FORCE_REINSTALL} ]]; then
        APT_INSTALL_CMD="${APT_INSTALL_CMD} --reinstall"
    fi

    # Run the apt-get install
    CMD="${APT_INSTALL_CMD} ${APT_PACKAGES}";
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to install packages via apt-get - ${APT_PACKAGES}";
    fi
    print_stage_success_done;

    # Post install configs
    write_apt_package_configs "${FILE_PATH}";
}


install_apt_package ${@};
