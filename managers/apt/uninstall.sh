#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


uninstall_installed_apt_package() {
    FILE_PATH=${@};
    if [[ -z ${APT_UNINSTALL_CMD} ]]; then
        echo "false";
        return;
    fi
    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.apt_package_info" );
    INSTALLED_APT_PACKAGES=$(read_json_data "${PACKAGE_INFO}" ".apt_packages" | tr -d '"');

    print_stage_header "Removing apt package(s) - '${INSTALLED_APT_PACKAGES}'";
    echo

    # Run the apt-get install
    CMD="${APT_UNINSTALL_CMD} ${INSTALLED_APT_PACKAGES}";
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to remove apt package(s) - ${INSTALLED_APT_PACKAGES}";
    fi
    print_stage_success_done;
}


uninstall_installed_apt_package "${@}";
