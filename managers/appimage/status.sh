#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


# Source the utils.sh file for required helper functions
source ${SCRIPT_DIR}/utils.sh


check_installed_appimage_package() {
    FILE_PATH=${@};
    PACKAGE_BIN=$(read_package_binary "${FILE_PATH}");

    # Ensure we have all required config information
    if [[ "${PACKAGE_BIN}" == 'null' ]]; then
        throw_exception "Package configuration incomplete. Missing key: '.bin'";
    fi

    # Appimage install location
    DEST=${HOME}/.local/share/${PACKAGE_BIN};

    # If it exists
    if [[ -e "${DEST}" ]]; then
        PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.appimage_package_info");

        # Set defaults for destinations of appimage, icon and version number
        VERSION="unknown"
        DEST_FILE=${DEST}/${PACKAGE_BIN}-${VERSION}.Appimage;

        # Get source information
        i=1;
        for item in $(read_appimage_package_data "${PACKAGE_INFO}"); do
            item="$(echo ${item} | base64 --decode)";
            if [[ "${i}" == "1" ]]; then
                if [[ "${item}" != 'null' ]]; then
                    VERSION="${item}";
                fi
            fi
            if [[ "${i}" == "2" ]]; then
                if [[ "${item}" != 'null' ]]; then
                    DOWNLOAD_LINK="${item}";
                fi
            fi
            if [[ "${i}" == "3" ]]; then
                if [[ "${item}" != 'null' ]]; then
                    DEST_FILE="${DEST}/${item}";
                fi
            fi
            ((i++))
        done

        # Check if the appimage destination exists
        if [[ -e ${DEST_FILE} ]]; then
            echo "true";
            return;
        fi
    fi
    echo "false";
}


check_installed_appimage_package ${@};
