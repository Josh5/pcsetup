#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


# Source the utils.sh file for required helper functions
source ${SCRIPT_DIR}/utils.sh


# Install an appimage package
install_appimage_package() {
    FILE_PATH="";
    FORCE_REINSTALL='false';
    for ARG in ${@}; do
        if [[ "${ARG}" == "--reinstall" || "${ARG}" == "-r" ]]; then
            FORCE_REINSTALL='true';
            continue;
        else
            FILE_PATH="${ARG}";
            continue;
        fi
    done

    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    PACKAGE_BIN=$(read_package_binary "${FILE_PATH}");
    PACKAGE_DESCRIPTION=$(read_package_description "${FILE_PATH}");
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.appimage_package_info");
    APPIMAGE_SRC=$(read_json_data "${PACKAGE_INFO}" ".source");
    APPIMAGE_DESKTOP_SHORTCUT=$(read_json_data "${PACKAGE_INFO}" ".desktop_shortcut");
    APPIMAGE_ICON_URL=$(read_json_data "${PACKAGE_INFO}" ".icon_url");
    APPIMAGE_ICON_DEST=$(read_json_data "${PACKAGE_INFO}" ".icon_dest");

    print_stage_header "Installing appimage package - '${PACKAGE_NAME}'"
    echo

    # Ensure we have all required config information
    if [[ "${PACKAGE_BIN}" == 'null' ]]; then
        throw_exception "Package configuration incomplete. Missing key: '.bin'";
    fi
    if [[ "${APPIMAGE_SRC}" == 'null' ]]; then
        throw_exception "Package configuration incomplete. Missing key: '.installation.appimage_package_info.source'";
    fi

    # Make destination
    DEST=${HOME}/.local/share/${PACKAGE_BIN};
    bash -c "mkdir -p ${DEST}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to make destination folder - ${DEST}";
    fi

    # Set defaults for destinations of appimage, icon and version number
    VERSION="unknown"
    DEST_FILE=${DEST}/${PACKAGE_BIN}-${VERSION}.Appimage;
    DEST_ICON=${DEST}/${APPIMAGE_ICON_DEST};

    # Get source information
    i=1;
    for item in $(read_appimage_package_data "${PACKAGE_INFO}"); do
        item="$(echo ${item} | base64 --decode)";
        if [[ "${i}" == "1" ]]; then
            if [[ "${item}" != 'null' ]]; then
                VERSION="${item}";
            fi
        fi
        if [[ "${i}" == "2" ]]; then
            if [[ "${item}" != 'null' ]]; then
                DOWNLOAD_LINK="${item}";
            fi
        fi
        if [[ "${i}" == "3" ]]; then
            if [[ "${item}" != 'null' ]]; then
                DEST_FILE="${DEST}/${item}";
            fi
        fi
        ((i++))
    done

    # Ensure we have a link
    if [[ -z ${DOWNLOAD_LINK} ]]; then
        throw_exception "Failed to find appimage download link for - ${PACKAGE_NAME}";
    fi

    # Check if we already have the latest version
    if [[ -e ${DEST_FILE} && -z ${FORCE_REINSTALL} ]]; then
        echo "Appimage ${PACKAGE_NAME} is already the newest version (${VERSION})";
        return;
    fi

    # Download package appimage
    echo "Downloading ${PACKAGE_NAME} appimage from ${DOWNLOAD_LINK}";
    CMD="curl -L '${DOWNLOAD_LINK}' --output '${DEST_FILE}'";
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to download appimage for - ${PACKAGE_NAME}";
    fi
    echo

    # Extract tar
    if [[ "${DEST_FILE}" == *".tar."* ]]; then
        CWD=${PWD};
        cd ${DEST};

        if [[ "${DEST_FILE}" == *".tar.gz" ]]; then
            CMD="tar xfz ${DEST_FILE} --strip 1 -C ./";
        fi
        if [[ "${DEST_FILE}" == *".tar.bz2" ]]; then
            CMD="tar xjf ${DEST_FILE} --strip 1 -C ./";
        fi
        if [[ "${DEST_FILE}" == *".tar.xz" ]]; then
            CMD="tar xaf ${DEST_FILE} --strip 1 -C ./";
        fi

        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to extract tar file - ${DEST_FILE}";
        fi
        echo

        # Overwrite destination file name
        TAR_BIN_NAME=$(read_json_data "${PACKAGE_INFO}" ".tar_bin_name");
        DEST_FILE=${DEST}/${TAR_BIN_NAME};

        cd ${CWD};
    fi

    # Download package icon (if one is configured)
    if [[ "${APPIMAGE_ICON_URL:-null}" != 'null' && "${APPIMAGE_ICON_DEST:-null}" != 'null' ]]; then
        echo "Downloading ${PACKAGE_NAME} icon from ${APPIMAGE_ICON_URL}";
        CMD="curl -L '${APPIMAGE_ICON_URL}' --output '${DEST_ICON}'";
        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to download icon for - ${PACKAGE_NAME}";
        fi
        echo
    fi

    # Make appimage executable
    if [[ -e "${DEST_FILE}" ]]; then
        CMD="chmod +x ${DEST_FILE}";
        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to set permissions of appimage - ${PACKAGE_NAME}";
        fi
        echo
    fi

    # Write a command-line launcher
    LAUNCHER=${HOME}/.local/bin/${PACKAGE_BIN};
    if [[ -e "${DEST_FILE}" ]]; then
        mkdir -p ${HOME}/.local/bin
        CMD="rm -f ${LAUNCHER}";
        bash -c "${CMD}";
        echo "Linking executable cli launcher for appimage --> ${LAUNCHER}"
        echo
        CMD="ln -s ${DEST_FILE} ${LAUNCHER}";
        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to write cli launch command for appimage package - ${PACKAGE_NAME}";
        fi
        echo
    fi

    # Write a launcher script (fixes some applications)
    LAUNCHER_SCRIPT=${HOME}/.local/bin/${PACKAGE_BIN}.sh;
    if [[ -e "${DEST_FILE}" ]]; then
        mkdir -p ${HOME}/.local/bin
        CMD="rm -f ${LAUNCHER_SCRIPT}";
        bash -c "${CMD}";
        echo "Writing a launcher script for appimage --> ${LAUNCHER_SCRIPT}"
        echo
        touch ${LAUNCHER_SCRIPT};
        echo '#!/usr/bin/env bash' > ${LAUNCHER_SCRIPT};
        echo "${DEST_FILE} "'$@' >> ${LAUNCHER_SCRIPT};
        chmod +x ${LAUNCHER_SCRIPT};
        echo
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to write launcher script for appimage package - ${PACKAGE_NAME}";
        fi
        echo
    fi

    # Write a start menu link for gnome
    if [[ "${APPIMAGE_DESKTOP_SHORTCUT}" != 'null' && ! $(grep -ri "${DEST}" ${HOME}/.local/share/applications/) ]]; then
        mkdir -p ${HOME}/.local/share/applications
        menu_shortcut=${HOME}/.local/share/applications/${APPIMAGE_DESKTOP_SHORTCUT};
        touch ${menu_shortcut};
        echo '[Desktop Entry]' > ${menu_shortcut};
        echo 'Name='${PACKAGE_NAME} >> ${menu_shortcut};
        echo 'Exec="'${LAUNCHER_SCRIPT}'" %U' >> ${menu_shortcut};
        echo 'Comment="'${PACKAGE_DESCRIPTION}'"' >> ${menu_shortcut};
        echo 'Icon='${DEST_ICON} >> ${menu_shortcut};
        echo 'Type=Application' >> ${menu_shortcut};
        echo 'TryExec='${LAUNCHER} >> ${menu_shortcut};
        echo 'Terminal=false' >> ${menu_shortcut};
        chmod 755 ${menu_shortcut};
        echo
    fi

    print_stage_success_done;
}


install_appimage_package ${@};
