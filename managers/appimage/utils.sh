#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Read the appimage package data
read_appimage_package_data() {
    PACKAGE_INFO=${@};
    VERSION='null';
    DOWNLOAD_LINK='null';
    DEST_FILE='null';

    APPIMAGE_SRC=$(read_json_data "${PACKAGE_INFO}" ".source");
    if [[ "${APPIMAGE_SRC}" == "github" ]]; then
        # Get download form github
        GITHUB_PROJECT=$(read_json_data "${PACKAGE_INFO}" ".github_project_url");
        GITHUB_RELEASE_EXT=$(read_json_data "${PACKAGE_INFO}" ".github_release_extension");

        # Get latest version information from github
        VERSION=$(github_get_latest_release "${GITHUB_PROJECT}");
        if [[ -z ${VERSION} ]]; then
            throw_exception "Unable to find latest appimage version on git for - ${PACKAGE_NAME}";
        fi

        # Get matching download link from github
        DOWNLOAD_LINK=$(github_get_release_download_url "${GITHUB_PROJECT}" "${VERSION}" "${GITHUB_RELEASE_EXT}");

        # Overwrite destination file name
        DEST_FILE="${DOWNLOAD_LINK##*/}";
    fi
    if [[ "${APPIMAGE_SRC}" == "url" ]]; then
        IMAGE_URL=$(read_json_data "${PACKAGE_INFO}" ".image_url");
        IMAGE_VERSION_CMD=$(read_json_data "${PACKAGE_INFO}" ".image_version_cmd");

        # Get latest version information from github
        VERSION=$(bash -c "${IMAGE_VERSION_CMD}");
        if [[ -z ${VERSION} ]]; then
            throw_exception "Unable to find latest appimage version for - ${PACKAGE_NAME}";
        fi

        # Replace version information in download URL
        DOWNLOAD_LINK=$(echo "${IMAGE_URL}" | sed 's|${VERSION}|'${VERSION}'|g');

        # Overwrite destination file name
        DEST_FILE=${PACKAGE_BIN}-${VERSION}.AppImage;
    fi
    if [[ "${APPIMAGE_SRC}" == "tar" ]]; then
        TAR_URL=$(read_json_data "${PACKAGE_INFO}" ".tar_url");
        TAR_EXT=$(read_json_data "${PACKAGE_INFO}" ".tar_extension");
        TAR_VERSION_CMD=$(read_json_data "${PACKAGE_INFO}" ".tar_version_cmd");

        # Get latest version information from github
        VERSION=$(bash -c "${TAR_VERSION_CMD}");
        if [[ -z ${VERSION} ]]; then
            throw_exception "Unable to find latest appimage version for - ${PACKAGE_NAME}";
        fi

        # Replace version information in download URL
        DOWNLOAD_LINK=$(echo "${TAR_URL}" | sed 's|${VERSION}|'${VERSION}'|g');

        # Overwrite destination file name
        DEST_FILE=${PACKAGE_BIN}-${VERSION}.${TAR_EXT};
    fi
    echo "$(echo ${VERSION} | base64 -w0) $(echo ${DOWNLOAD_LINK} | base64 -w0) $(echo ${DEST_FILE} | base64 -w0)";
}
