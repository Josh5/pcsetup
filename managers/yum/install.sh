#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


# Install a package via apt
install_yum_package() {
    FILE_PATH="";
    FORCE_REINSTALL='false';
    for ARG in ${@}; do
        if [[ "${ARG}" == "--reinstall" || "${ARG}" == "-r" ]]; then
            FORCE_REINSTALL='true';
            continue;
        else
            FILE_PATH="${ARG}";
            continue;
        fi
    done

    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.yum_package_info" );
    YUM_PACKAGES=$(read_json_data "${PACKAGE_INFO}" ".yum_packages" | tr -d '"');

    print_stage_header "Installing package(s) via yum - '${YUM_PACKAGES}'";
    echo

    # Install rpm package
    CMD="${SUDO} ${YUM_INSTALL_CMD} ${YUM_PACKAGES}";
    if [[ ${FORCE_REINSTALL} == "true" ]]; then
        CMD="${SUDO} ${YUM_REINSTALL_CMD} ${YUM_PACKAGES}";
    fi

    # Run the yum install command
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to install packages via yum - ${APT_PACKAGES}";
    fi
    print_stage_success_done;
}


install_yum_package ${@};
