#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done



# Write a executable launcher file in /usr/local/bin/
write_flatpak_package_launcher() {
    FILE_PATH=${@};
    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    PACKAGE_BIN=$(read_package_binary "${FILE_PATH}");
    PACKAGE_RUN_CMD=$(read_json_data "${PACKAGE_INFO}" ".run_command" | tr -d '"');
    if [[ "${PACKAGE_BIN}" != 'null' && "${PACKAGE_RUN_CMD}" != 'null' ]]; then
        LAUNCHER=/usr/local/bin/${PACKAGE_BIN};
        CMD="${SUDO} rm -f ${LAUNCHER}";
        bash -c "${CMD}";
        echo "Writing executable launcher for flatpak --> ${LAUNCHER}"
        echo
        CMD="echo '#!/bin/bash' | ${SUDO} tee -a ${LAUNCHER}";
        bash -c "${CMD}";
        CMD="echo '${PACKAGE_RUN_CMD}' | ${SUDO} tee -a ${LAUNCHER}";
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to write launch command for flatpak package - ${PACKAGE_NAME}";
        fi
        echo
        CMD="${SUDO} chmod +x /usr/local/bin/${PACKAGE_BIN}";
        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to set permissions of launch command for flatpak package - ${PACKAGE_NAME}";
        fi
        print_stage_success_done;
    fi
}

# Install a package via flatpak
install_flatpak_package() {
    FILE_PATH="";
    FORCE_REINSTALL='false';
    for ARG in ${@}; do
        if [[ "${ARG}" == "--reinstall" || "${ARG}" == "-r" ]]; then
            FORCE_REINSTALL='true';
            continue;
        else
            FILE_PATH="${ARG}";
            continue;
        fi
    done

    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.flatpak_package_info");
    PACKAGE_INSTALL_LOCATION=$(read_json_data "${PACKAGE_INFO}" ".source" | tr -d '"');
    PACKAGE_INSTALL_NAME=$(read_json_data "${PACKAGE_INFO}" ".flatpak_name" | tr -d '"');

    print_stage_header "Installing package via flatpak - '${PACKAGE_INSTALL_NAME}'"
    echo
    CMD="${FLATPAK_INSTALL_CMD} ${PACKAGE_INSTALL_LOCATION}";
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to install package via flatpak - ${PACKAGE_INSTALL_NAME}";
    fi
    print_stage_success_done;

    # Write a command-line launcher
    write_flatpak_package_launcher "${FILE_PATH}";
}


install_flatpak_package ${@};
