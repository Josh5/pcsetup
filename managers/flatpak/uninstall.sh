#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


# Unstall package
uninstall_installed_flatpak_package() {
    FILE_PATH=${@};
    if [[ -z ${FLATPAK_UNINSTALL_CMD} ]]; then
        echo "false";
        return;
    fi
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.flatpak_package_info");
    FLATPAK_PACKAGE_NAME=$(read_json_data "${PACKAGE_INFO}" ".flatpak_name" | tr -d '"');

    print_stage_header "Removing flatpak package - '${FLATPAK_PACKAGE_NAME}'";
    echo

    # Run the flatpak uninstall
    CMD="${FLATPAK_UNINSTALL_CMD} ${FLATPAK_PACKAGE_NAME}";
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to remove flatpak package - ${FLATPAK_PACKAGE_NAME}";
    fi
    print_stage_success_done;

    # Remove launcher
    PACKAGE_BIN=$(read_package_binary "${FILE_PATH}");
    LAUNCHER=/usr/local/bin/${PACKAGE_BIN};
    if [[ "${PACKAGE_BIN}" != 'null' && -e "${LAUNCHER}" ]]; then
        print_stage_header "Removing flatpak package launcher - '${LAUNCHER}'";
        echo
        CMD="${SUDO} rm -f ${LAUNCHER}";
        echo ${CMD};
        echo
        bash -c "${CMD}";
        if [[ $? -ne 0 ]]; then
            throw_exception "Failed to remove launch command for flatpak package - ${FLATPAK_PACKAGE_NAME}";
        fi
        print_stage_success_done;
    fi
}


uninstall_installed_flatpak_package "${@}";
