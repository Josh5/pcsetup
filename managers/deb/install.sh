#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 09:47:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


# Install a deb package via apt
install_deb_package() {
    FILE_PATH="";
    FORCE_REINSTALL='false';
    for ARG in ${@}; do
        if [[ "${ARG}" == "--reinstall" || "${ARG}" == "-r" ]]; then
            FORCE_REINSTALL='true';
            continue;
        else
            FILE_PATH="${ARG}";
            continue;
        fi
    done

    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    PACKAGE_BIN=$(read_package_binary "${FILE_PATH}");
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.deb_package_info");
    DEB_URL=$(read_json_data "${PACKAGE_INFO}" ".deb_url" | tr -d '"');
    DEB_VERSION_CMD=$(read_json_data "${PACKAGE_INFO}" ".deb_version_cmd");
    DEB_URL_CMD=$(read_json_data "${PACKAGE_INFO}" ".deb_url_cmd");

    print_stage_header "Installing package via deb package - '${DEB_URL}'"
    echo

    # Set defaults for destinations of appimage, icon and version number
    VERSION="unknown"
    DOWNLOAD_LINK=${DEB_URL};
    DEST_FILE="/tmp/${DOWNLOAD_LINK##*/}.deb";

    # Replace default version information
    if [[ "${DEB_VERSION_CMD}" != 'null' ]]; then
        VERSION=$(${DEB_VERSION_CMD});
        DOWNLOAD_LINK=$(echo "${DOWNLOAD_LINK}" | sed 's|${VERSION}|'${VERSION}'|g');
        DEST_FILE="/tmp/${DOWNLOAD_LINK##*/}.deb";
    fi
    # Replace URL if a command to fetch one was provided
    if [[ "${DEB_URL_CMD}" != 'null' ]]; then
        DEB_URL_CMD=$(echo "${DEB_URL_CMD}" | sed 's|${VERSION}|'${VERSION}'|g');
        DOWNLOAD_LINK=$(${DEB_URL_CMD});
        DEST_FILE="/tmp/${DOWNLOAD_LINK##*/}.deb";
    fi

    # Check if we already have the latest version
    CHECK_INSTALLED_VERSION=$(echo ${VERSION} | sed "s/[^[:digit:].-]//g"); # <- remove v from start of version numbers
    if [[ $(dpkg -l | grep ${PACKAGE_BIN} | grep ${CHECK_INSTALLED_VERSION}) && -z ${FORCE_REINSTALL} ]]; then
        echo "${PACKAGE_NAME} is already the newest version (${CHECK_INSTALLED_VERSION})";
        return;
    fi

    # Download deb package
    echo "Downloading ${PACKAGE_NAME} deb package from ${DOWNLOAD_LINK}";
    CMD="${SUDO} curl -L '${DOWNLOAD_LINK}' --output '${DEST_FILE}'";
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to download deb package for - ${PACKAGE_NAME}";
    fi
    echo

    # Install deb package
    CMD="${SUDO} apt install -y ${DEST_FILE}";
    if [[ ! -z ${FORCE_REINSTALL} ]]; then
        CMD="${CMD} --reinstall"
    fi
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to install deb package - ${PACKAGE_NAME}";
    fi
    print_stage_success_done;
}


install_deb_package ${@};
