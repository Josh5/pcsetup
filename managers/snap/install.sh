#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Set script path
SCRIPT_DIR=$( dirname $( readlink -f ${BASH_SOURCE[0]} ) );


# Source all utils scripts
for util in ${SCRIPT_DIR}/../../utils/*.sh; do
    source ${util};
done


# Install a package via snap
# TODO: Add reinstall
install_snap_package() {
    FILE_PATH="";
    FORCE_REINSTALL='false';
    for ARG in ${@}; do
        if [[ "${ARG}" == "--reinstall" || "${ARG}" == "-r" ]]; then
            FORCE_REINSTALL='true';
            continue;
        else
            FILE_PATH="${ARG}";
            continue;
        fi
    done

    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    PACKAGE_BIN=$(read_package_binary "${FILE_PATH}");
    PACKAGE_INFO=$(read_json_data "${FILE_PATH}" ".installation.snap_package_info");
    PACKAGE_INSTALL_CMD=$(read_json_data "${PACKAGE_INFO}" ".install_cmd" | tr -d '"');

    print_stage_header "Installing package via snap - '${PACKAGE_NAME}'"
    echo
    CMD="${SNAP_INSTALL_CMD} ${PACKAGE_INSTALL_CMD}";
    echo ${CMD};
    echo
    bash -c "${CMD}";
    if [[ $? -ne 0 ]]; then
        throw_exception "Failed to install package via snap - ${PACKAGE_NAME}";
    fi
    print_stage_success_done;
}


install_snap_package ${@};
