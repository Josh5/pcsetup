#!/bin/bash
#
# @Author: Josh Sunnex
# @Date:   2018-08-13 12:54:13
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-03-17 11:50:19
# 
# bash -c "$(curl -sSL https://gist.githubusercontent.com/Josh5/14bbb2e69b701693fee5790c7f5989b3/raw/SETUP.sh)";


###########
## Manual commands:
##
MANUAL_COMMANDS='''

# Setup gnome terminal theme
bash -c "$(curl -fsSL https://raw.githubusercontent.com/josh5/gnome-terminal-one/master/one-dark.sh)";

'''
##
##########


SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
RELEASE=$(lsb_release -sr);
RELEASE_MAJOR=$(lsb_release -sr | awk -F '.' '{print $1}');
CODENAME=$(lsb_release -sc);
DATE_STAMP=$(date +"%Y-%m-%d_%H-%M-%S");
APT_ARGS="-y";
FORCE_REINSTALL="NO";
if [[ "${RELEASE_MAJOR}" -ge "18" ]]; then
    APT_ARGS+=" -n";
fi

###########
## CONFIG
##
CONFIG_URL_APT_UPDATE="https://gist.githubusercontent.com/Josh5/14bbb2e69b701693fee5790c7f5989b3/raw/setup_ubuntu_apt.sh";
CONFIG_URL_DPKG_GET="https://gist.githubusercontent.com/Josh5/f5a3e764b1a6a461cd437c02ab97c66e/raw/dpkg-get";
##
##########

APT_PACKAGES="";
APT_INSTALL_CMD="sudo apt-get install -y ";
SNAP_PACKAGES="";
SNAP_INSTALL_CMD="sudo snap install ";
DEB_PACKAGES="";
DEB_INSTALL_CMD="sudo dpkg-get ";

if [[ $(which snap) ]]; then
    USE_SNAP_PACKAGES="true";
else
    USE_SNAP_PACKAGES="false";
fi


# Manipulate OS values to allow for distros like LinuxMint
OS_ID=$(lsb_release -si | awk '{print tolower($0)}');
RELEASE=$(lsb_release -sr);
RELEASE_MAJOR=$(lsb_release -sr | awk -F '.' '{print $1}');
CODENAME=$(lsb_release -sc);
DATE_STAMP=$(date +"%Y-%m-%d_%H-%M-%S");
if [[ ${OS_ID} =~ "mint" ]]; then
    OS_ID="linuxmint";
    # Get Ubuntu codename for this release
    CODENAME=$(cat /etc/os-release | grep UBUNTU_CODENAME | awk -F'=' '{print $2}');
    # Dont use snap packages with Linux Mint
    USE_SNAP_PACKAGES="false";
fi


get_install_commands() {
    re='(?<=^__install_)(.+?)(?=\()';
    commands=$(cat ${BASH_SOURCE[0]} | grep -o -P ${re});
    echo ${commands};
}

# Update apt sources list:
update_apt_sources() {
    stage_header "Updating apt sources...";
    if [[ -f "${SCRIPT_DIR}/setup_ubuntu_apt.sh" ]]; then
        sudo bash -c "$(cat ${SCRIPT_DIR}/setup_ubuntu_apt.sh)";
    else
        sudo bash -c "$(curl -sSL ${CONFIG_URL_APT_UPDATE})";
    fi
    echo
    echo -e "\e[93m";
    res=$(user_input "Continue with the rest of the System Setup? (y|n)" "TF")
    echo 
    echo -e "\e[0m";
    if [[ "${res##*$'\n'}" == "0" ]]; then
        echo "Exiting...";
        exit 0;
    fi
}

# Install dpkg-get script:
setup_dpkg_get() {
    stage_header "Installing dpkg-get...";
    if [[ -f "${SCRIPT_DIR}/dpkg-get" ]]; then
        sudo bash -c "cp -fv ${SCRIPT_DIR}/dpkg-get /usr/local/bin/dpkg-get && \
            chmod a+x /usr/local/bin/dpkg-get";
    else
        sudo bash -c "curl -L '${CONFIG_URL_DPKG_GET}' > /usr/local/bin/dpkg-get && \
            chmod a+x /usr/local/bin/dpkg-get";
    fi
}

# Install script dependencies:
setup_script_dependencies() {
    APT_TO_INSTALL="";
    [[ ! -x $(which curl) ]] && APT_TO_INSTALL="${APT_TO_INSTALL} curl";
    [[ ! -x $(which jq) ]] && APT_TO_INSTALL="${APT_TO_INSTALL} jq";
    [[ ! -x $(which wget) ]] && APT_TO_INSTALL="${APT_TO_INSTALL} wget";
    [[ ! -x $(which unrar) ]] && APT_TO_INSTALL="${APT_TO_INSTALL} unrar";
    [[ ! -x $(which unzip) ]] && APT_TO_INSTALL="${APT_TO_INSTALL} unzip";
    [[ ! -x $(which zip) ]] && APT_TO_INSTALL="${APT_TO_INSTALL} zip";
    if [[ "${APT_TO_INSTALL}" != "" ]]; then
        stage_header "Installing script dependencies collection...";
        sudo apt-get update;
        sudo apt-get install -y ${APT_TO_INSTALL};
    fi
    [[ ! -x $(which dpkg-get) || ${FORCE_REINSTALL} == "YES" ]] && setup_dpkg_get;
}

# Get latest release tag from github
get_latest_git_release() {
    REPO="${1}";
    curl --silent "https://api.github.com/repos/${REPO}/releases/latest" \
        | grep '"tag_name":' \
        | sed -E 's/.*"([^"]+)".*/\1/';
}

# Get all release tags from github
get_all_git_release() {
    REPO="${1}";
    curl --silent "https://api.github.com/repos/${REPO}/releases" \
        | grep '"tag_name":' \
        | sed -E 's/.*"([^"]+)".*/\1/';
}

# Get all release tags from github
get_git_release_download_url() {
    REPO="${1}";
    TAG="${2}";
    TYPE="${3}";
    curl --silent "https://api.github.com/repos/${REPO}/releases/tags/${TAG}" \
        | grep '"browser_download_url":' \
        | sed -E 's/.*"([^"]+)".*/\1/' \
        | grep "${TYPE}";
}


# 
#  /$$$$$$                       /$$               /$$ /$$        /$$$$$$                               
# |_  $$_/                      | $$              | $$| $$       /$$__  $$                              
#   | $$   /$$$$$$$   /$$$$$$$ /$$$$$$    /$$$$$$ | $$| $$      | $$  \ $$  /$$$$$$   /$$$$$$   /$$$$$$$
#   | $$  | $$__  $$ /$$_____/|_  $$_/   |____  $$| $$| $$      | $$$$$$$$ /$$__  $$ /$$__  $$ /$$_____/
#   | $$  | $$  \ $$|  $$$$$$   | $$      /$$$$$$$| $$| $$      | $$__  $$| $$  \ $$| $$  \ $$|  $$$$$$ 
#   | $$  | $$  | $$ \____  $$  | $$ /$$ /$$__  $$| $$| $$      | $$  | $$| $$  | $$| $$  | $$ \____  $$
#  /$$$$$$| $$  | $$ /$$$$$$$/  |  $$$$/|  $$$$$$$| $$| $$      | $$  | $$| $$$$$$$/| $$$$$$$/ /$$$$$$$/
# |______/|__/  |__/|_______/    \___/   \_______/|__/|__/      |__/  |__/| $$____/ | $$____/ |_______/ 
#                                                                         | $$      | $$                
#                                                                         | $$      | $$                
#                                                                         |__/      |__/                
# 



# Install base dependencies:
__install_base_dependencies() {
    if [[ ${1} == "stage" ]]; then
        if [[ "${RELEASE_MAJOR}" -ge "18" ]]; then
            APT_PACKAGES="${APT_PACKAGES} \
                make build-essential flex pv \
                git mercurial subversion \
                vim lzma cpio whois \
                perl python python-markdown \
                python-pip python3-pip \
                python3-setuptools python-setuptools \
                python-dev python3-dev \
                software-properties-common \
                nfs-common ukuu \
                apt-transport-https \
                ca-certificates \
                google-drive-ocamlfuse \
                ubuntu-restricted-extras \
                ubuntu-make \
                unison rsync \
                openssh-server \
                wiggle npm \
                golang-go";
        else
            APT_PACKAGES="${APT_PACKAGES} \
                make build-essential flex pv \
                git-core mercurial subversion \
                vim lzma cpio whois \
                perl python python-markdown \
                python-pip python3-pip \
                python3-setuptools python-setuptools \
                python-software-properties \
                nfs-common ukuu \
                google-drive-ocamlfuse \
                adobe-flashplugin \
                ubuntu-restricted-extras \
                ubuntu-make \
                unison rsync \
                openssh-server \
                wiggle npm";
        fi
    fi
}

# Install chrome depends:
__install_chrome() {
    if [[ ${1} == "stage" ]]; then
        _install="YES";
        if [[ $(which google-chrome) ]]; then
            res=$(user_input "Chrome is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            APT_PACKAGES="${APT_PACKAGES} \
                xbase-clients xvfb python-psutil";
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb";
        fi
    fi
}

# Install Cinnamon Desktop:
__install_cinnamon_desktop() {
    if [[ ! $(lsb_release -si | awk '{print tolower($0)}') =~ "mint" ]]; then
        if [[ ${1} == "stage" ]]; then
            APT_PACKAGES="${APT_PACKAGES} \
                cinnamon-desktop-environment";
        fi
    fi
}

# Install video playback deps:
__install_codecs() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            libdvdnav4 libdvdread4 gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly libdvd-pkg";
    fi
    if [[ ${1} == "config" ]]; then
        sudo dpkg-reconfigure libdvd-pkg
    fi
}

# Install DBeaver CE:
__install_dbeaver() {
    if [[ ${1} == "stage" ]]; then
        _install="YES";
        if [[ $(which dbeaver) ]]; then
            res=$(user_input "DBeaver CE is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb";
        fi
    fi
}

# Install DEV Tools:
__install_devtools() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            meld fasd git repo";
        _install="YES";
        if [[ $(which bat) ]]; then
            res=$(user_input "Bat is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://github.com/sharkdp/bat/releases/download/v0.6.0/bat_0.6.0_amd64.deb";
        fi
    fi
}
    
# Install Discord depends:
__install_discord() {
    if [[ ${1} == "stage" ]]; then
        if [[ ${USE_SNAP_PACKAGES} == "true" ]]; then
            SNAP_PACKAGES="${SNAP_PACKAGES} \
                discord";
        else
            APT_PACKAGES="${APT_PACKAGES} \
                libc++1 libc++abi1";
            _install="YES";
            if [[ $(which discord) ]]; then
                res=$(user_input "Discord is already installed, would you like to re-install it? (y|n)" "TF");
                if [[ "${res##*$'\n'}" == "0" ]]; then
                    _install="NO";
                fi
                echo
            fi
            if [[ ${_install} == "YES" ]]; then
                DEB_PACKAGES="${DEB_PACKAGES} \
                    https://discordapp.com/api/download?platform=linux";
            fi
        fi
    fi
}

# Install Docker
__install_docker() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            docker-ce";
    fi
    if [[ ${1} == "config" ]]; then
        sudo groupadd docker;
        sudo gpasswd -a ${USER} docker;
        mkdir -p /home/${USER}/.docker;
        sudo chown ${USER}:${USER} /home/${USER}/.docker -R;
    fi
}

# Install Dropbox
__install_dropbox() {
    if [[ ${1} == "stage" ]]; then
        _install="YES";
        if [[ $(which dropbox) ]]; then
            res=$(user_input "Dropbox is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://www.dropbox.com/download?dl=packages/ubuntu/dropbox_2015.10.28_amd64.deb";
        fi
    fi
}

# Install Etcher
__install_etcher() {
    if [[ ${1} == "stage" ]]; then
        INSTALL_ETCHER="YES";
        if [[ $(which etcher) ]]; then
            res=$(user_input "Etcher is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                INSTALL_ETCHER="NO";
            fi
            echo
        fi
    fi
    if [[ ${1} == "custom_install" && ${INSTALL_ETCHER} == "YES" ]]; then
        sudo mkdir -p /opt/Etcher;
        etcher_version=$(get_latest_git_release "balena-io/etcher");
        etcher_link=$(get_git_release_download_url "balena-io/etcher" "${etcher_version}" "x86_64.AppImage");
        etcher_dest_file=/opt/Etcher/balena-etcher-electron-${etcher_version}-x86_64.AppImage;
        echo "${etcher_link}";
        sudo curl -L "${etcher_link}" --output ${etcher_dest_file};
        sudo chmod +x ${etcher_dest_file};
        sudo rm -f /opt/Etcher/etcher;
        sudo ln -s ${etcher_dest_file} /opt/Etcher/etcher;
        sudo rm -f /usr/local/bin/etcher;
        sudo ln -s /opt/Etcher/etcher /usr/local/bin/etcher;
        sudo curl -L "https://github.com/balena-io/etcher/raw/master/assets/icon.png" --output /opt/Etcher/icon.png;
        if [[ ! $(grep -ri "/opt/Etcher" ${HOME}/.local/share/applications/) ]]; then
            mkdir -p ${HOME}/.local/share/applications
            menu_shortcut=${HOME}/.local/share/applications/appimagekit-balenaEtcher.desktop;
            touch ${menu_shortcut};
            echo '[Desktop Entry]' > ${menu_shortcut};
            echo 'Name=balenaEtcher' >> ${menu_shortcut};
            echo 'Exec="/opt/Etcher/balena-etcher-electron-v1.4.9-x86_64.AppImage" %U' >> ${menu_shortcut};
            echo 'Comment=Flash OS images to SD cards and USB drives, safely and easily.' >> ${menu_shortcut};
            echo 'Icon=/opt/Etcher/icon.png' >> ${menu_shortcut};
            echo 'Type=Application' >> ${menu_shortcut};
            echo 'X-Desktop-File-Install-Version=0.23' >> ${menu_shortcut};
            echo 'X-AppImage-Comment=Generated by ./bin//balena-etcher-electron.wrapper' >> ${menu_shortcut};
            echo 'TryExec=/opt/Etcher/balena-etcher-electron-v1.4.9-x86_64.AppImage' >> ${menu_shortcut};
            chmod 755 ${menu_shortcut};
        fi
        sudo chmod -Rf a+w /opt/Etcher;
    fi
}

# Install Firefox Dev Edition
__install_firefox_dev() {
    if [[ ${1} == "stage" ]]; then
        INSTALL_FIREFOX_DEV="YES";
        if [[ $(which firefox-developer) ]]; then
            res=$(user_input "Firefox Dev Edition is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                INSTALL_FIREFOX_DEV="NO";
            fi
            echo
        fi
    fi
    if [[ ${1} == "custom_install" && ${INSTALL_FIREFOX_DEV} == "YES" ]]; then
        echo 'y' | umake web firefox-dev --lang=en-US /home/${USER}/.local/share/umake/web/firefox-dev;
    fi
}

# Install franz:
__install_franz() {
    if [[ ${1} == "stage" ]]; then
        _install="YES";
        if [[ $(which franz) ]]; then
            res=$(user_input "Franz is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://github.com/meetfranz/franz/releases/download/v5.0.0-beta.18/franz_5.0.0-beta.18_amd64.deb";
        fi
    fi
}

# Install FS Tools:
__install_fs_tools() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            cryptsetup bindfs";
    fi
    if [[ ${1} == "config" ]]; then
        sudo sed -i s/#user_allow_other/user_allow_other/ /etc/fuse.conf
    fi
}

# Install GIMP:
__install_gimp() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            gimp";
    fi
}

# Install Jax Liberty:
__install_jaxx_liberty() {
    jaxx_version=2.0.5;
    jaxx_liberty_file=/opt/JaxxLiberty/com.liberty.jaxx-${jaxx_version}-x86_64.AppImage;
    jaxx_dl_url="https://github.com/Jaxx-io/JaxxLiberty/releases/download/v${jaxx_version}/com.liberty.jaxx-${jaxx_version}-x86_64.AppImage";
    if [[ ${1} == "stage" ]]; then
        INSTALL_JAXX="YES";
        if [[ -e ${jaxx_liberty_file} ]]; then
            res=$(user_input "Jaxx Liberty is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                INSTALL_JAXX="NO";
            fi
            echo
        fi
    fi
    if [[ ${1} == "custom_install" && ${INSTALL_JAXX} == "YES" ]]; then
        sudo mkdir -p /opt/JaxxLiberty;
        sudo rm -rf /opt/JaxxLiberty/*;
        sudo rm -f /usr/local/bin/jaxxliberty;
        echo "${jaxx_dl_url}";
        sudo curl -L "${jaxx_dl_url}" --output ${jaxx_liberty_file};
        sudo curl -L "https://jaxx.io/img/Jaxx_Primary_Icon_RGB_Screeen.svg" --output /opt/JaxxLiberty/Jaxx_Primary_Icon.svg;
        sudo chmod +x ${jaxx_liberty_file};
        sudo ln -s ${jaxx_liberty_file} /opt/JaxxLiberty/jaxxliberty;
        sudo ln -s /opt/JaxxLiberty/jaxxliberty /usr/local/bin/jaxxliberty;
        if [[ ! $(grep -ri "/opt/JaxxLiberty" ${HOME}/.local/share/applications/) ]]; then
            mkdir -p ${HOME}/.local/share/applications
            menu_shortcut=${HOME}/.local/share/applications/appimagekit-com.liberty.jaxx.desktop;
            touch ${menu_shortcut};
            echo "[Desktop Entry]" > ${menu_shortcut};
            echo "Name=Jaxx Liberty" >> ${menu_shortcut};
            echo "Comment=Blockchain platform" >> ${menu_shortcut};
            echo "Exec=\"${jaxx_liberty_file}\" %U" >> ${menu_shortcut};
            echo "Terminal=false" >> ${menu_shortcut};
            echo "Type=Application" >> ${menu_shortcut};
            echo "Icon=appimagekit-com.liberty.jaxx" >> ${menu_shortcut};
            echo "StartupWMClass=Jaxx Liberty" >> ${menu_shortcut};
            echo "X-AppImage-Version=${jaxx_version}" >> ${menu_shortcut};
            echo "Categories=Utility;" >> ${menu_shortcut};
            echo "X-Desktop-File-Install-Version=0.23" >> ${menu_shortcut};
            echo "X-AppImage-Comment=Generated by /tmp/.mount_com.liSafiOF/AppRun" >> ${menu_shortcut};
            echo "TryExec=${jaxx_liberty_file}" >> ${menu_shortcut};

        fi
        sudo chmod -Rf a+w /opt/JaxxLiberty;
    fi
}

# Install Keepass:
__install_keepass() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            keepassxc";
    fi
}

# Install MEGAsync:
__install_megasync() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            libcrypto++6 libmediainfo0v5 libtinyxml2-6 libzen0v5";
        _install="YES";
        if [[ $(which megasync) ]]; then
            res=$(user_input "MEGAsync is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://mega.nz/linux/MEGAsync/xUbuntu_18.04/amd64/megasync-xUbuntu_18.04_amd64.deb";
        fi
    fi
}

# Install MySQL Workbench
__install_mysql_workbench() {
    if [[ ${1} = "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            mysql-workbench python3-keyring mysql-client";
    fi
}

# Install Nginx
__install_nginx() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            nginx";
    fi
}

# Install PGAdmin3
__install_pgadmin() {
    if [[ ${1} = "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            pgadmin3 postgresql-client postgresql-client-common";
    fi
}

# Install php
__install_php() {
    if [[ ${1} = "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            php-common php-cli php-fpm php-pear";
    fi
}

# Install PhpStorm
__install_phpstorm() {
    if [[ ${1} == "stage" ]]; then
        INSTALL_PHPSTORM="YES";
        if [[ $(which phpstorm) ]]; then
            res=$(user_input "PhpStorm is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                INSTALL_PHPSTORM="NO";
            fi
            echo
        fi
    fi
    if [[ ${1} == "custom_install" && ${INSTALL_PHPSTORM} == "YES" ]]; then
        DOWNLOADS_DATA=$(curl -sSL -X GET 'https://data.services.jetbrains.com/products/releases?code=PS&latest=true&type=release' \
            -H 'Accept: application/json, text/javascript, */*; q=0.01' \
            --insecure \
            --compressed \
            | jq -c '.PS' \
            );
        phpstorm_link=$(echo ${DOWNLOADS_DATA} | jq -c .[0].downloads.linux.link | tr -d '"');
        phpstorm_version=PhpStorm-$(echo ${DOWNLOADS_DATA} | jq -c .[0].version| tr -d '"');
        echo "${phpstorm_link}";
        curl -L ${phpstorm_link} --output /tmp/${phpstorm_version}.tar.gz;
        sudo rm -rf /opt/PhpStorm*;
        sudo rm -f /usr/local/bin/phpstorm;
        sudo tar xfz /tmp/${phpstorm_version}.tar.gz -C /opt/;
        sudo mv /opt/PhpStorm-* /opt/PhpStorm;
        sudo ln -s /opt/PhpStorm/bin/phpstorm.sh /usr/local/bin/phpstorm;
        if [[ ! $(grep -ri "/opt/PhpStorm" ${HOME}/.local/share/) ]]; then
            menu_shortcut=${HOME}/.local/share/applications/phpstorm.desktop;
            touch ${menu_shortcut};
            echo "[Desktop Entry]" > ${menu_shortcut};
            echo "Name=PhpStorm" >> ${menu_shortcut};
            echo "Exec=/opt/PhpStorm/bin/phpstorm.sh" >> ${menu_shortcut};
            echo "Comment=/opt/PhpStorm" >> ${menu_shortcut};
            echo "Terminal=false" >> ${menu_shortcut};
            echo "Icon=/opt/PhpStorm/bin/phpstorm.png" >> ${menu_shortcut};
            echo "Type=Application" >> ${menu_shortcut};
        fi
        sudo chmod -Rf a+w /opt/PhpStorm;
    fi
}

# Install Shutter:
__install_shutter() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            shutter libextutils-depends-perl libextutils-pkgconfig-perl pkg-config";
        if [[ ! $(which shutter) ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://launchpad.net/ubuntu/+archive/primary/+files/libgoocanvas-common_1.0.0-1_all.deb \
                https://launchpad.net/ubuntu/+archive/primary/+files/libgoocanvas3_1.0.0-1_amd64.deb \
                https://launchpad.net/ubuntu/+archive/primary/+files/libgoo-canvas-perl_0.06-2ubuntu3_amd64.deb";
        fi
    fi
}

# Install Skype
__install_skype() {
    if [[ ${1} == "stage" ]]; then
        _install="YES";
        if [[ $(which skypeforlinux) ]]; then
            res=$(user_input "Skype is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://go.skype.com/skypeforlinux-64.deb";
        fi
    fi
}

# Install Slack
__install_slack() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            gir1.2-gnomekeyring-1.0";
        _install="YES";
        if [[ $(which slack) ]]; then
            res=$(user_input "Slack is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://downloads.slack-edge.com/linux_releases/slack-desktop-3.2.1-amd64.deb";
        fi
    fi
}

# Install Sublime Text
__install_sublime() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            sublime-text";
    fi
}

# Install TaskWarrior
__install_taskwarrior() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            taskwarrior";
    fi
}
    
# Install TeamViewer depends:
__install_teamviewer() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            libdouble-conversion1 libqt5core5a libqt5dbus5 libqt5gui5 libqt5network5 libqt5opengl5 \
            libqt5printsupport5 libqt5qml5 libqt5quick5 libqt5sql5 libqt5sql5-sqlite libqt5svg5 libqt5webkit5 \
            libqt5widgets5 libqt5x11extras5 libxcb-xinerama0 qml-module-qtgraphicaleffects qml-module-qtquick-controls \
            qml-module-qtquick-dialogs qml-module-qtquick-layouts qml-module-qtquick-privatewidgets qml-module-qtquick-window2 \
            qml-module-qtquick2 qt5-gtk-platformtheme qtdeclarative5-qtquick2-plugin qttranslations5-l10n";
        _install="YES";
        if [[ $(which teamviewer) ]]; then
            res=$(user_input "TeamViewer is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://download.teamviewer.com/download/linux/teamviewer_amd64.deb";
        fi
    fi
}

# Install TortoiseHG
__install_tortoisehg() {
    if [[ ${1} == "stage" ]]; then
        APT_PACKAGES="${APT_PACKAGES} \
            tortoisehg";
    fi
}

# Install Tizonia
__install_tizonia() {
    if [[ ${1} == "stage" ]]; then
        if [[ ${USE_SNAP_PACKAGES} == "true" ]]; then
            SNAP_PACKAGES="${SNAP_PACKAGES} \
                tizonia";
        fi
    fi
    if [[ ${1} == "custom_install" ]]; then
        if [[ ${USE_SNAP_PACKAGES} == "false" ]]; then
            sudo -H python2.7 -m pip install --upgrade pip;
            sudo -H python3 -m pip install --upgrade pip;
            sudo -H python2.7 -m pip install python-Levenshtein;
            sudo -H python3 -m pip install python-Levenshtein;
            sudo bash -c "$(curl -sSL https://github.com/tizonia/tizonia-openmax-il/raw/master/tools/install.sh)";
        fi
    fi
}


# Install VSCode
__install_vscode() {
    if [[ ${1} == "stage" ]]; then
        _install="YES";
        if [[ $(which code) ]]; then
            res=$(user_input "VSCode is already installed, would you like to re-install it? (y|n)" "TF");
            if [[ "${res##*$'\n'}" == "0" ]]; then
                _install="NO";
            fi
            echo
        fi
        if [[ ${_install} == "YES" ]]; then
            DEB_PACKAGES="${DEB_PACKAGES} \
                https://vscode-update.azurewebsites.net/latest/linux-deb-x64/stable";
        fi
    fi
}

# Install Wine
__install_wine() {
    if [[ ${1} == "stage" ]]; then
        # Remove vineyard - There has to be a better tool than that for installing and managing windows applications
        APT_PACKAGES="${APT_PACKAGES} \
            wine-stable wine64 wine32";
    fi
}

# Cleanup any missing packages or deps and update anything available
cleanup_and_upgrade() {
    stage_header "Install updates and any missing dependencies...";
    sudo apt-get --fix-broken install -y # Just incase a dep was missed
    # Update everything else:
    sudo apt-get update
    sudo apt-get upgrade -y
}

# Cleanup any missing packages or deps and update anything available
reboot() {
    echo -e "\e[91m";
    echo -e "Your machine will reboot in 5 sec. Pres CTRL+C to cancel...";
    echo -e "\e[0m";
    sleep 5;
    sudo reboot;
    exit 0;
}




user_input() {
    echo;
    echo -e "\e[93m";
    RES=0;
    while [[ true ]]; do
        read -n1 -p "${1}  " an 
        case ${an} in  
            y|Y)
                RES=1;
                echo -e "\e[0m";
                if [[ ${2} != "TF" ]]; then
                    ${2}
                fi
                break
                ;; 
            n|N) 
                RES=0;
                break
                ;; 
            *) 
                echo
                echo 'Please select either "y" or "n"'
                ;; 
        esac
        echo
    done
    echo -e "\e[0m";
    echo 
    if [[ "${2}" == "TF" ]]; then
        echo ${RES};
    fi
}

install_packages() {
    functions="$(get_install_commands)";
    stage_header "Stage install list...";
    echo
    for func in ${functions}; do
        if [[ ! -z ${INSTALL_SINGLE_APP} && ${INSTALL_SINGLE_APP} != *${func}* ]]; then
            continue
        fi
        __install_${func} stage;
    done
    if [[ ! -z ${APT_PACKAGES} ]]; then
        stage_header "Install APT Packages...";
        echo
        echo ${APT_PACKAGES};
        echo
        ${APT_INSTALL_CMD} ${APT_PACKAGES};
        echo
    fi
    if [[ ! -z ${SNAP_PACKAGES} ]]; then
        stage_header "Install SNAP packages...";
        echo
        echo ${SNAP_PACKAGES};
        echo 
        ${SNAP_INSTALL_CMD} ${SNAP_PACKAGES};
        echo
    fi
    if [[ ! -z ${DEB_PACKAGES} ]]; then
        stage_header "Install DEB packages...";
        echo
        echo ${DEB_PACKAGES};
        echo 
        for deb in ${DEB_PACKAGES}; do
            echo
            echo ${deb};
            ${DEB_INSTALL_CMD} ${deb};
        done
        echo
    fi
    stage_header "Run custom install scripts...";
    echo
    for func in ${functions}; do
        if [[ ! -z ${INSTALL_SINGLE_APP} && ${INSTALL_SINGLE_APP} != *${func}* ]]; then
            continue
        fi
        __install_${func} custom_install;
    done
    echo
    stage_header "Configure installation...";
    echo
    for func in ${functions}; do
        if [[ ! -z ${INSTALL_SINGLE_APP} && ${INSTALL_SINGLE_APP} != *${func}* ]]; then
            continue
        fi
        __install_${func} config;
    done

}

main() {
    sudo echo;
    setup_script_dependencies;
    user_input "[APT SOURCES] Would you like to first update apt sources? (this will create a backup in /etc/ if anything goes wrong) (y|n)" update_apt_sources;
    install_packages;
    cleanup_and_upgrade;
    user_input "[REBOOT] Your system needs to be rebooted, would you like to do that now? (y|n)" reboot;


    echo 
    echo 
    echo -e "\e[92m";
    echo -e "Some changes may require that you reboot the machine. It is recommended that you do this before getting started with any productivity.";
    echo
    echo -e "Your system is now setup and ready to go...";
    echo -e "\e[0m";
}

help() {
    echo -n "

This is a System Setup script written by Josh.5
This script will setup (along with a bunch of deps):
    
    Install apps:
        - Chrome (optional)
        - DBeaver CE
        - Discord
        - Docker
        - Dropbox
        - Etcher
        - Firefox Dev Edition (optional)
        - Franz
        - GIMP
        - Jax Liberty
        - Keepass
        - MEGAsync
        - MySQL Workbench
        - Nginx
        - PGAdmin3
        - PHP
        - PhpStorm
        - Shutter
        - Skype
        - Slack
        - Sublime Text 3
        - TaskWarrior
        - TeamViewer (optional)
        - TortoiseHG
        - Tizonia
        - VSCode
        - Wine

    Install deps:
        - adobe-flash
        - bindfs
        - cryptsetup
        - curl
        - dpkg-get
        - flex
        - git
        - make
        - mercurial
        - nfs-common
        - perl
        - python
        - unrar
        - Video codecs
        - whois
        - ubuntu-make
        - Ubuntu's Restricted Extras
        - Ukuu

    Configure System:
        - Setup nemo desktop for Gnome3 (optional)
        - Setup Cinnamon desktop (optional)
    ";
    echo
}

prompt_start() {
    help;
    echo
    if [[ ! -z ${INSTALL_SINGLE_APP} ]]; then
        echo "Installing application(s) ${INSTALL_SINGLE_APP}";
        install_packages;
    else
        user_input "Would you like to continue? (y|n)" main;
    fi
    echo
}

stage_header() {
    echo -e "\e[1;34m"
    echo -e "####################################################################"
    echo -e "##                "
    echo -e "##       ${1}";
    echo -e "##                "
    echo -e "####################################################################"
    echo -e "\e[0m"
}




for arg in ${@}; do
    if [[ "${arg}" =~ "--reinstall" ]]; then
        APT_INSTALL_CMD="${APT_INSTALL_CMD} --reinstall ";
        FORCE_REINSTALL="YES";
        continue;
    fi
    if [[ "${arg}" =~ "--app" ]]; then
        INSTALL_SINGLE_APP="${INSTALL_SINGLE_APP} $(echo ${arg} | awk -F'=' '{print $2}')";
        continue;
    fi
    if [[ "${arg}" =~ "help" ]]; then
        help;
        exit 0;
    fi
done

prompt_start;

exit 0;
