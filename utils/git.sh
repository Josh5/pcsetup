#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 

# Get latest release tag from github
github_get_latest_release() {
    REPO="${1}";
    curl --silent "https://api.github.com/repos/${REPO}/releases/latest" \
        | grep '"tag_name":' \
        | sed -E 's/.*"([^"]+)".*/\1/';
}

# Get all release tags from github
github_get_all_release() {
    REPO="${1}";
    curl --silent "https://api.github.com/repos/${REPO}/releases" \
        | grep '"tag_name":' \
        | sed -E 's/.*"([^"]+)".*/\1/';
}

# Get all release tags from github
github_get_release_download_url() {
    REPO="${1}";
    TAG="${2}";
    TYPE="${3}";
    curl --silent "https://api.github.com/repos/${REPO}/releases/tags/${TAG}" \
        | grep '"browser_download_url":' \
        | sed -E 's/.*"([^"]+)".*/\1/' \
        | grep -P "${TYPE}" \
        | head -n1;
}