#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


# Read json data from file
read_json_data() {
    SRC="${1}";
    JQ_QUERY="${@:2}";
    if [[ -e ${SRC} ]]; then
        echo $(cat ${SRC} | jq -r "${JQ_QUERY}");
    else
        echo $(echo ${SRC} | jq -r "${JQ_QUERY}");
    fi
}

# Ensure that the package exists
package_exists() {
    PACKAGE="${@}";
    SELECTED_PACKAGES="";
    # Find all packages matching a regex
    for package in $(list_all_packages); do

        if [[ $(echo "$package" | grep -P  "^${PACKAGE}$") ]]; then
            SELECTED_PACKAGES="${SELECTED_PACKAGES} ${package}";
            continue;
        fi

    done
    if [[ -z ${SELECTED_PACKAGES} ]]; then
        echo "false";
    else
        echo "${SELECTED_PACKAGES}";
    fi
}

# List all packages
list_all_packages() {
    packages="";
    for item in $( find ${SCRIPT_DIR}/packages/ -name "*.json" -type f  -print0 | sort -z | xargs -r0 echo ); do
        item=$( basename $(echo $item | sed 's/\.json$//') );
        packages="${packages} 
            ${item}"
    done
    echo "${packages}";
}

# Read a single package's name
read_package_name() {
    FILE_PATH="${1}";
    echo $(read_json_data "${FILE_PATH}" ".name" | tr -d '"');
}

# Read a single package's binary name
read_package_binary() {
    FILE_PATH="${1}";
    echo $(read_json_data "${FILE_PATH}" ".bin" | tr -d '"');
}

# Read a single package's binary description
read_package_description() {
    FILE_PATH="${1}";
    echo $(read_json_data "${FILE_PATH}" ".description" | tr -d '"');
}

# Check if for first package compatibility with this Linux Distro
check_package_install_compatibility() {
    FILE_PATH=${@};
    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    INSTALL_DATA=$(read_json_data "${FILE_PATH}" ".installation");
    INSTALL_METHODS=$(read_json_data "${FILE_PATH}" ".installation.install_methods");
    for row in $(echo "${INSTALL_METHODS}" | jq -r '.[] | @base64'); do
        METHOD="$(echo ${row} | base64 --decode)";

        if [[ "${METHOD}" == "apt" && ! -z ${APT_INSTALL_CMD} ]]; then
            echo "apt";
            return;
        fi

        if [[ "${METHOD}" == "pacman" && ! -z ${PACMAN_INSTALL_CMD} ]]; then
            echo "pacman";
            return;
        fi

        if [[ "${METHOD}" == "flatpak" && ! -z ${FLATPAK_INSTALL_CMD} ]]; then
            echo "flatpak";
            return;
        fi

        if [[ "${METHOD}" == "snap" && ! -z ${SNAP_INSTALL_CMD} ]]; then
            echo "snap";
            return;
        fi

        if [[ "${METHOD}" == "appimage" ]]; then
            echo "appimage";
            return;
        fi

        if [[ "${METHOD}" == "deb" && ! -z ${APT_INSTALL_CMD} ]]; then
            echo "deb";
            return;
        fi

        if [[ "${METHOD}" == "rpm" && ! -z ${YUM_INSTALL_CMD} ]]; then
            echo "rpm";
            return;
        fi

        if [[ "${METHOD}" == "src" ]]; then
            echo "src";
            return;
        fi

        if [[ "${METHOD}" == "yum" && ! -z ${YUM_INSTALL_CMD} ]]; then
            echo "yum";
            return;
        fi
    done
    echo "false";
}

# Check if a package is installed, return all the methods it is installed with
check_package_is_installed() {
    FILE_PATH=${@};
    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    INSTALL_DATA=$(read_json_data "${FILE_PATH}" ".installation");
    INSTALL_METHODS=$(read_json_data "${FILE_PATH}" ".installation.install_methods");

    INSTALLED_METHODS='';
    for row in $(echo "${INSTALL_METHODS}" | jq -r '.[] | @base64'); do
        METHOD="$(echo ${row} | base64 --decode)";
        #res=$(check_installed_${METHOD}_package ${FILE_PATH});
        res=$(bash ${SCRIPT_DIR}/managers/${METHOD}/status.sh ${FILE_PATH});
        if [[ "${res}" == "true" ]]; then
            INSTALLED_METHODS="${INSTALLED_METHODS} ${METHOD}";
        fi
    done
    echo "${INSTALLED_METHODS}";
}

# Reads all package data and installs the first compatible method
process_package_data_and_install() {
    FILE_PATH=${@};
    PACKAGE_NAME=$(read_package_name "${FILE_PATH}");
    INSTALL_DATA=$(read_json_data "${FILE_PATH}" ".installation");
    INSTALL_METHODS=$(read_json_data "${FILE_PATH}" ".installation.install_methods");

    # Check for best install method
    METHOD=$(check_package_install_compatibility "${FILE_PATH}");

    if [[ ${METHOD} == "false" ]]; then
        INCOMPATIBLE_METHODS=$(read_json_data "${INSTALL_METHODS}" ".[]");
        throw_exception "No compatible install method found for package '${PACKAGE_NAME}'. Configured methods: '${INCOMPATIBLE_METHODS}'";
    else
        #CMD="install_${METHOD}_package ${FILE_PATH}";
        ARGS="";
        if [[ ! -z ${FORCE_REINSTALL} && ${FORCE_REINSTALL} == "true" ]]; then
            ARGS="${ARGS} --reinstall"
        fi
        CMD="bash ${SCRIPT_DIR}/managers/${METHOD}/install.sh ${FILE_PATH} ${ARGS}";
        ${CMD};
    fi
    echo
}

# Loop through all packages and list them
list_and_print_all_packages() {
    SELECTED_PACKAGES=${@};
    if [[ -z ${SELECTED_PACKAGES} || "${SELECTED_PACKAGES}" == "ALL_PACKAGES" ]]; then
        SELECTED_PACKAGES=$(list_all_packages);
    fi
    # Fetch all packages available to install
    echo
    echo "Package(s) Status:";
    echo
    i=1
    for package in ${SELECTED_PACKAGES}; do

        if [[ "${package}" == "ALL_PACKAGES" ]]; then
            throw_exception "Invalid parameter passed with package selection - '--all'";
        fi

        counter=${i};
        if [[ "${i}" -lt "100" ]]; then
            counter=" ${i}";
        fi
        if [[ "${i}" -lt "10" ]]; then
            counter="  ${i}";
        fi
        FILE_PATH=${SCRIPT_DIR}/packages/${package}.json;
        PACKAGE_NAME=$(read_package_name ${FILE_PATH});

        # Check for best install method
        PREFERRED_METHOD=$(check_package_install_compatibility "${FILE_PATH}");
        if [[ "${PREFERRED_METHOD}" == "false" ]]; then
            PREFERRED_METHOD="${CLRED}INCOMPATIBLE${CNORM}"
        fi

        # Check if package is installed
        INSTALLED_METHODS=$(check_package_is_installed "${FILE_PATH}");
        IS_INSTALLED='';
        if [[ ! -z ${INSTALLED_METHODS} ]]; then
            IS_INSTALLED="${CLGREEN}"
        fi
        if [[ -f "${FILE_PATH}" ]]; then
            echo -e "${IS_INSTALLED}      ${counter}) ${PACKAGE_NAME}";
            echo -e "${IS_INSTALLED}              Package Config: ${package}";
            echo -e "${IS_INSTALLED}              Preferred install method: ${PREFERRED_METHOD}";
            if [[ ! -z ${INSTALLED_METHODS} ]]; then
            echo -e "${IS_INSTALLED}              Installed method(s): ${INSTALLED_METHODS}";
            fi
            echo -e "${CNORM}";
        fi
        ((i++))
    done
    echo
}

# Loop through all packages and install them all (in order of dependences)
install_collection_of_packages() {
    SELECTED_PACKAGES=${@};
    FILES='';
    PACKAGES='';
    if [[ "${SELECTED_PACKAGES}" == "ALL_PACKAGES" ]]; then
        # Fetch all packages available for install
        for package in $(list_all_packages); do

            # Get path to package data
            FILE_PATH=${SCRIPT_DIR}/packages/${package}.json;

            # Check for best install method
            METHOD=$(check_package_install_compatibility "${FILE_PATH}");

            if [[ ${METHOD} != "false" ]]; then
                PACKAGE_NAME=$(read_package_name ${FILE_PATH});
                PACKAGES="${PACKAGES} $(echo ${PACKAGE_NAME} | base64 -w0)";
                FILES="${FILES} ${FILE_PATH}";
            fi

        done
    else
        # Fetch configs of the selected packages for install
        for package in ${SELECTED_PACKAGES}; do

            if [[ "${package}" == "ALL_PACKAGES" ]]; then
                throw_exception "Invalid parameter passed with package selection - '--all'";
            fi

            # Get path to package data
            FILE_PATH=${SCRIPT_DIR}/packages/${package}.json;

            if [[ -e ${FILE_PATH} ]]; then
                # Check for best install method
                METHOD=$(check_package_install_compatibility "${FILE_PATH}");

                if [[ ${METHOD} != "false" ]]; then
                    PACKAGE_NAME=$(read_package_name ${FILE_PATH});
                    PACKAGES="${PACKAGES} $(echo ${PACKAGE_NAME} | base64 -w0)";
                    FILES="${FILES} ${FILE_PATH}";
                fi
            else
                throw_exception "Invalid package selection - '${package}'";
            fi

        done
    fi

    print_package_header ${PACKAGES};
    echo
    echo
    echo
    for FILE_PATH in ${FILES}; do
        process_package_data_and_install ${FILE_PATH};
    done
    echo
}

# List all packages to select a single one to install along with any dependences
install_a_single_package_from_select_options() {
    conf_array='';
    # Fetch all packages available for install
    echo
    echo "Available package install configurations:";
    echo
    i=1
    k=0
    for package in $(list_all_packages); do
        package_path=${SCRIPT_DIR}/packages/${package}.json;
        package_name=$(read_package_name ${package_path});
        if [[ -f "${package_path}" ]]; then
            echo "      ${i}) ${package_name}";
            conf_array[ $k ]="${package}.json" 
        fi
        ((i++))
        ((k++))
    done
    echo
    if [[ ${conf_array} ]]; then # Only show if list not empty
        echo
        read -p "Select a package from the list above: " menu
        numbers='^[0-9]+$'
        if [[ "${menu}" -le "${#conf_array[@]}" && ${menu} =~ ${numbers} && "${menu}" != "0" ]]; then
            (( menu-- ))
            selected_package=${conf_array[${menu}]}
            echo;
        else
            echo; echo "That was not a valid selection."; echo "No packages will be installed.";
            exit 1
        fi
    else
        echo "No packages were found."; echo "No packages will be installed.";
    fi
    echo

    install_package packages/${selected_package};

    exit 0;
}

# Read a single package's data
install_package() {
    FILE_PATH="${1}";
    PACKAGE_NAME=$(read_package_name ${FILE_PATH});

    print_package_header $(echo ${PACKAGE_NAME} | base64 -w0);
    echo
    echo
    echo
    process_package_data_and_install ${FILE_PATH};
    echo
}

# Uninstall a selection of packages (space separated list)
uninstall_collection_of_packages() {
    SELECTED_PACKAGES=${@};
    FILES='';
    PACKAGES='';
    if [[ "${SELECTED_PACKAGES}" == "ALL_PACKAGES" ]]; then
        throw_exception "Invalid parameter combination '--all, --uninstall'";
    fi

    # Fetch configs of the selected packages for install
    for package in ${SELECTED_PACKAGES}; do

        if [[ "${package}" == "ALL_PACKAGES" ]]; then
            throw_exception "Invalid parameter combination '--all, --uninstall'";
        fi

        # Get path to package data
        FILE_PATH=${SCRIPT_DIR}/packages/${package}.json;

        if [[ -e ${FILE_PATH} ]]; then

            # Check if package is installed
            INSTALLED_METHODS=$(check_package_is_installed "${FILE_PATH}");
            for METHOD in ${INSTALLED_METHODS}; do
                CMD="uninstall_installed_${METHOD}_package ${FILE_PATH}";
                ${CMD};
            done
        else
            throw_exception "Invalid package selection - '${package}'";
        fi

    done

    exit 0;
}
