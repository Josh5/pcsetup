#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-11 18:46:42
# 


# Set date stamp
DATE_STAMP=$(date +"%Y-%m-%d_%H-%M-%S");


# Set Colours
CHEAD="\e[93m";
CPATCH="\e[92m";
CLBLUE="\e[94m";
CLMAG="\e[95m";
CLRED="\e[91m";
CLGREEN="\e[92m";
CMODIFIED="\e[1;94m"; # M Light Blue (bold)
CUNTRACKED="\e[1;4;95m"; # ? Light Mag (bold underlined)
CMISSING="\e[1;4;36m"; # ! Cyan (bold underlined)
CREMOVED="\e[1;91m"; # R Light Red (bold)
CADDED="\e[1;92m"; # A Light Green (bold)
CNORM="\e[0m";


# Check if we need superuser permissions
SUDO='';
if [[ $(command -v sudo) ]]; then
    SUDO='sudo';
    if [[ ${EUID} == 0 ]]; then
        SUDO='';
    else
        for ARG in ${@}; do
            if [[ "${ARG}" == "--no-sudo" ]]; then
                SUDO='';
                break;
            fi
        done
    fi
fi


# Check package managers
if [[ $(command -v apt-get) ]]; then
    APT_INSTALL_CMD="${SUDO} apt-get install -y ";
    APT_INSTALL_REPO_CMD="${SUDO} add-apt-repository -y ";
    APT_INSTALL_REPO_KEY_CMD="curl -fsSL {@} | ${SUDO} apt-key add - ";
    APT_UNINSTALL_CMD="${SUDO} apt remove -y ";
fi
if [[ $(command -v pacman) ]]; then
    PACMAN_INSTALL_CMD="${SUDO} pacman --noconfirm -S ";
    PACMAN_INSTALL_REPO_KEY_CMD="curl -fsSL {@} | ${SUDO} pacman-key --add - ";
    PACMAN_INSTALL_REPO_KEY_ID_CMD="${SUDO} pacman-key --lsign-key {@} ";
    PACMAN_REFRESH_REPOS_CMD="${SUDO} pacman -Sy ";
fi
if [[ $(command -v yum) ]]; then
    YUM_INSTALL_CMD="${SUDO} yum -y install ";
    YUM_REINSTALL_CMD="${SUDO} yum -y reinstall ";
    YUM_UNINSTALL_CMD="${SUDO} yum -y remove ";
    YUM_REFRESH_REPOS_CMD="${SUDO} yum update ";
fi
if [[ $(command -v flatpak) ]]; then
    FLATPAK_INSTALL_CMD="${SUDO} flatpak install -y ";
    FLATPAK_UNINSTALL_CMD="${SUDO} flatpak uninstall -y ";
fi
if [[ $(command -v snap) ]]; then
    SNAP_PACKAGES="";
    SNAP_INSTALL_CMD="${SUDO} snap install ";
fi


# Check distribution
OS_ID=$(cat /etc/os-release | grep -P '^ID=' | awk -F'=' '{print $2}' | awk '{print tolower($0)}');
OS_ID_LIKE=$(cat /etc/os-release | grep -P '^ID_LIKE=' | awk -F'=' '{print $2}' | awk '{print tolower($0)}');
RELEASE=$(cat /etc/os-release | grep -P '^VERSION_ID=' | awk -F'=' '{print $2}' | tr -d '"');
RELEASE_MAJOR=$(cat /etc/os-release | grep -P '^VERSION_ID=' | awk -F'=' '{print $2}' | tr -d '"');
CODENAME=$(cat /etc/os-release | grep -P '^VERSION_CODENAME=' | awk -F'=' '{print $2}' | tr -d '"');
UBUNTU_CODENAME=$(cat /etc/os-release | grep -P '^UBUNTU_CODENAME=' | awk -F'=' '{print $2}' | awk '{print tolower($0)}');


# Also manipulate OS values to allow for distros like LinuxMint
if [[ ! -z ${OS_ID_LIKE} ]]; then
    OS_ID=${OS_ID_LIKE};
fi
if [[ ! -z ${UBUNTU_CODENAME} ]]; then
    CODENAME=${UBUNTU_CODENAME};
fi
