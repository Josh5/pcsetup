#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


print_package_header() {
    PACKAGES="${@}";
    echo -ne "${CLBLUE}";
    echo 
    echo "#######################################################################################################";
    echo "###  #############################################################################################  ###";
    echo "###                                                                                                 ###";
    echo "###     Installing Packages:                                                                        ###";
    echo "###                                                                                                 ###";
    echo "###                                                                                                 ###";
    for row in ${PACKAGES}; do
        PACKAGE="$(echo ${row} | base64 --decode)";
        printf "###  ${CLGREEN}`tput bold` %-90s `tput sgr0`${CLBLUE}   ###\n" "               + ${PACKAGE}"
    done
    echo "###                                                                                                 ###";
    echo "###  #############################################################################################  ###";
    echo "#######################################################################################################";
    echo 
    echo -ne "${CNORM}";
}

print_stage_header() {
    echo -ne "${CHEAD}";
    echo
    echo "#######################################################################################################";
    echo "###";
    echo "###  ${@}";
    echo "###";
    echo
    echo -ne "${CNORM}";
}

print_stage_success_done() {
    echo -ne "${CLGREEN}";
    echo
    echo "      |Done|"
    echo
    echo -ne "${CNORM}";
}