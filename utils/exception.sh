#!/bin/bash
#
# @Author: Josh.5
# @Date:   2019-08-04 10:00:00
# @Last Modified by:   Josh.5
# @Last Modified time: 2019-08-04 09:47:00
# 


throw_exception() {
    # TODO: Add TC syntax to here also
    echo
    echo -ne "${CLRED}";
    echo "+------------------------------------------------------------------------------------------------------+"
    echo "|                                                                                                      |"
    printf "|`tput bold` %-100s `tput sgr0`${CLRED}|\n" "!!! ERROR !!!"
    echo "|                                                                                                      |"
    printf "| %-100s ${CLRED}|\n" "$@"
    echo "|                                                                                                      |"
    echo "+------------------------------------------------------------------------------------------------------+"
    echo -ne "${CNORM}";
    echo
    exit 1;
}